using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getObject : MonoBehaviour
{
    public List<GameObject> list;


    public void onInstantiate()
    {
        for (int i = 0; i < list.Count; i++)
        {
            GameObject go = Instantiate(list[i], list[i].transform.position, list[i].transform.rotation);
        }
    }
}
