using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CodeUiData : MonoBehaviour
{
    public CodeSO codeSO; // Reference to the CodeSO scriptable object
    public TextMeshProUGUI yourTextMeshProUGUI;

    public CodeListSO codeListSO;
    private void OnEnable()
    {
        // Subscribe to the events
        if (codeSO != null)
        {
            codeSO.setInPlaceEvent += HandleSetInPlaceEvent;
            codeSO.setOutOfPlaceEvent += HandleSetOutOfPlaceEvent;
        }
    }

    private void OnDisable()
    {
        // Unsubscribe from the events
        if (codeSO != null)
        {
            codeSO.setInPlaceEvent -= HandleSetInPlaceEvent;
            codeSO.setOutOfPlaceEvent -= HandleSetOutOfPlaceEvent;
        }
    }

    private void HandleSetInPlaceEvent(CodeSO codeSOInstance)
    {
        // Handle the set in place event for the specific CodeSO instance
        Debug.Log("Object set in place: " + codeSOInstance.codeName);
        CodeSO code = codeListSO.getCodeByName(codeSOInstance.codeName);
        SetNewText(code.codeBody);
        // Add your logic here
    }

    private void HandleSetOutOfPlaceEvent(CodeSO codeSOInstance)
    {
        // Handle the set out of place event for the specific CodeSO instance
        Debug.Log("Object set out of place: " + codeSOInstance.codeName);
        // Add your logic here
    }

    public void SetNewText(string newText)
    {
        if (yourTextMeshProUGUI != null)
        {
            yourTextMeshProUGUI.text = newText;
        }
        else
        {
            Debug.LogError("TextMeshProUGUI component not assigned in the inspector.");
        }
    }
}