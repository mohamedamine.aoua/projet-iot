using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "ItemsListSO", menuName = "Final-Vr-Project/ItemsListSO", order = 3)]
public class ItemsListSO : ScriptableObject {
    
    [SerializeField] private List<ItemSO> items;

    public event UnityAction instantiateItem;

    public ItemSO getItemByName(string itemName)
    {
        foreach (ItemSO i in items)
        {
            if(i.itemName == itemName){return i;}
        }
        return null;
    }

    public void InvokeInstatiateItem()
    {
        if (instantiateItem != null)
        {
            instantiateItem.Invoke();
        }
    }
    public void InstatiateItem(string itemName)
    {   
        foreach(ItemSO i in items){
            if(i.itemName == itemName && !(i.isInstantiated))
            {
                GameObject it =Instantiate(i.itemPrefab);
                it.name = itemName;
                i.isInstantiated = true;
                break;
            }
            else{Debug.Log("item does not exist or already in the scene !");}
        }
    }
}