using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



[CreateAssetMenu(fileName = "CodeListSO1", menuName = "Final-Vr-Project/CodeListSO", order = 6)]
public class CodeListSO1 : ScriptableObject {
    
    [SerializeField] private List<CodeSO> Codes;

    public bool isInstantiated = false;

    public CodeSO getCodeByName(string codeName)
    {
        foreach (CodeSO c in Codes)
        {
            if(c.codeName == codeName ){return c;}
        }
        return null;
    }

    public void InstatiateCodes()
    {   
        int counter = 0;
        Vector3 p1 = new Vector3(6.33f,1.87f,-5.36f);
        Vector3 p2 = new Vector3(6.41f,1.87f,-4.88f);
        Vector3 p3 = new Vector3(6.41f,1.86f,-4.51f);
        Vector3 p4 = new Vector3(5.65f,1.86f,-5.16f);

        List<Vector3> positions = new List<Vector3>(); // Create a new List
        positions.Add(p1); // Add the Vector3 objects to the List
        positions.Add(p2);
        positions.Add(p3);
        positions.Add(p4);
        if(!isInstantiated){
            foreach(CodeSO c in Codes){

                GameObject cd =Instantiate(c.codePrefab);
                
                cd.transform.position = positions[counter];
                cd.name = c.codeName;
                counter++;
            }
            isInstantiated =true;
        }
        else{
            Debug.Log("codes already in the scene");
        }
    }

}
