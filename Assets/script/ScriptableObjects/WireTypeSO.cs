using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


[CreateAssetMenu(fileName = "WireType", menuName = "Final-Vr-Project/WireType", order = 0)]
public class WireTypeSO : ScriptableObject {
    public GameObject spherePrefab;
    public GameObject cylinderPrefab;
    public GameObject firstSocketPrefab;
    public GameObject secondSocketPrefab;
    public GameObject wirePrefab;
    public string WireName;
    public string firstItemAttached;
    public string secondItemAttached;
    public bool isInstantiated = false;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // This method will be called when the scene is loaded
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Reset the variable when a new scene is loaded
        isInstantiated = false;
    }

    // This method will be called when the scriptable object is destroyed
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}

