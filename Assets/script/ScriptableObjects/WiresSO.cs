using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "WiresSO", menuName = "Final-Vr-Project/WiresSO", order = 1)]
public class WiresSO : ScriptableObject {
    public List<WireTypeSO> wireTypes = new List<WireTypeSO>();   

    
    //Wire events
    public event UnityAction InstantiateWireEvent;
    
    public WireTypeSO getWireByName(string wireName)
    {
        foreach (WireTypeSO w in wireTypes)
        {
            if(w.WireName == wireName){return w;}
            Debug.Log(w.WireName);
        }
        return null;
    }

    public void InvokeInstantiateWireEvent()
    {
        if (InstantiateWireEvent != null)
        {
            InstantiateWireEvent.Invoke();
        }
    }

    public void InstantiteWire(string wireName)
    {

        WireTypeSO wireSO = getWireByName(wireName);
        if(!(wireSO.isInstantiated)){
            wireSO.wirePrefab.GetComponent<CableController>().SetWireType(wireSO);
            GameObject instantiatedPrefab = Instantiate(wireSO.wirePrefab, wireSO.wirePrefab.transform.position, wireSO.wirePrefab.transform.rotation);

            // Get the CableController component from the instantiated prefab
            CableController cableController = instantiatedPrefab.GetComponent<CableController>();
            wireSO.isInstantiated = true;

            if (cableController != null)
            {
                // Set the WireType attribute
                cableController.SetWireType(wireSO);
            }
            else
            {
                Debug.LogError("CableController component not found on the instantiated prefab.");
            }
        }
        else
        {
            Debug.Log("Cable already in the scene ! ");
        }
    }


}
