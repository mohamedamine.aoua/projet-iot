using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


[CreateAssetMenu(fileName = "ItemSO", menuName = "Final-Vr-Project/ItemSO", order = 2)]
public class ItemSO : ScriptableObject {
    public string itemName;
    public GameObject itemPrefab;
    public string itemDescription;
    public List<string> wiresToAttach;
    public List<string> ItemsToAttachDirectly;

    [SerializeField] private WiresSO wiresSO;
    
    [SerializeField] private ItemsListSO itemsListSO;

    //tracking attachment status 
    private bool isAttachedToItem{get; set;} = false;
    private bool isAttachedToWire{get; set;} = false;
    public bool isInstantiated = false;
    private GameObject attachedItem {get;set;}
    private GameObject attachedCable {get;set;}
    

    // Events for different attachment scenarios
    public event UnityAction attachToWireEvent;
    public event UnityAction attachToItemEvent;
    public event UnityAction unattachFromWireEvent;
    public event UnityAction unattachFromItemEvent;



    // Method to invoke attach events
    public void InvokeAttachToWireEvent()
    {
        if (attachToWireEvent != null)
        {
            attachToWireEvent.Invoke();
        }
    }

    public void InvokeAttachToItemEvent()
    {
        if (attachToItemEvent != null)
        {
            attachToItemEvent.Invoke();
        }
    }

    public void InvokeUnattachFromWireEvent()
    {
        if (unattachFromWireEvent != null)
        {
            unattachFromWireEvent.Invoke();
        }
    }

    public void InvokeUnattachFromItemEvent()
    {
        if (unattachFromItemEvent != null)
        {
            unattachFromItemEvent.Invoke();
        }
    }

    public void AttachToWire(string wireName)
    {
        if(!(this.isAttachedToWire) && (wiresToAttach.Contains(wireName)))
        {
            isAttachedToWire = true;
            attachedCable = wiresSO.getWireByName(itemName).wirePrefab;
            InvokeAttachToWireEvent();
        }
        else
        {
            Debug.Log("unable to attach to cable");
        }
    }

    // Method to set attachment details for item
    public void AttachToItem(string itemName)
    {
        if(!(this.isAttachedToItem) && ItemsToAttachDirectly.Contains(itemName) && (itemsListSO.getItemByName(itemName).isInstantiated))
        {
            isAttachedToItem = true;
            attachedItem = itemsListSO.getItemByName(itemName).itemPrefab;
            InvokeAttachToItemEvent();
        }
        else
        {
            Debug.Log("unable to attach to item");
        }

    }

    // Method to unattach from wire
    public void UnattachFromWire()
    {
        if(this.isAttachedToWire)
        {
            isAttachedToWire = false;
            attachedCable = null;
            InvokeUnattachFromWireEvent();
        }
        
    }

    // Method to unattach from item
    public void UnattachFromItem()
    {
        if(this.isAttachedToWire)
        {
            isAttachedToItem = false;
            attachedItem = null;
            InvokeUnattachFromItemEvent();
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // This method will be called when the scene is loaded
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Reset the variable when a new scene is loaded
        isInstantiated = false;
    }

    // This method will be called when the scriptable object is destroyed
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}