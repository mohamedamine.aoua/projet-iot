using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIItemButtonsHandler : MonoBehaviour
{
    public ItemsListSO itemsListSO;
    public string itemName;

    void Start()
    {
        // Attach button click listeners
        Button button = GetComponent<Button>();
        if (button != null)
        {
            button.onClick.AddListener(OnButtonClick);
        }
    }

    void OnButtonClick()
    {
        // Call the InstantiteWire method with the specified wireName
        itemsListSO.InstatiateItem(itemName);
    }
}


