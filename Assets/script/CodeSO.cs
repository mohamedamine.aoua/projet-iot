using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "CodeSO", menuName = "Final-Vr-Project/CodeSO", order = 4)]
public class CodeSO : ScriptableObject
{

    public string codeName;
    public GameObject codePrefab;
    public string codeBody;
    public bool isInPlace = false;

    // Pass the CodeSO instance as a parameter
    public event UnityAction<CodeSO> setInPlaceEvent;
    public event UnityAction<CodeSO> setOutOfPlaceEvent;


    public void InvokeSetInPlace()
    {
        if (setInPlaceEvent != null)
        {
            setInPlaceEvent.Invoke(this); // Pass 'this' as a parameter
        }
    }

    public void InvokeSetOutOfPlaceEvent()
    {
        if (setOutOfPlaceEvent != null)
        {
            setOutOfPlaceEvent.Invoke(this); // Pass 'this' as a parameter
        }
    }


    public void SetInPlace()
    {
        this.isInPlace = true;

    }

    public void SetOutOfPlace()
    {
        this.isInPlace = false;
    }
}