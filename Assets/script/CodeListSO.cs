using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



[CreateAssetMenu(fileName = "CodeListSO", menuName = "Final-Vr-Project/CodeListSO", order = 5)]
public class CodeListSO : ScriptableObject
{

    [SerializeField] private List<CodeSO> Codes;

    public bool isInstantiated = false;

    public CodeSO getCodeByName(string codeName)
    {
        foreach (CodeSO c in Codes)
        {
            if (c.codeName == codeName) { return c; }
        }
        return null;
    }

    public void InstatiateCodes()
    {
        int counter = 0;
        Vector3 p1 = new Vector3(-10.07f, 5.13f, 4.16f);
        Vector3 p2 = new Vector3(-11.14f, 5.13f, 4.16f);
        Vector3 p3 = new Vector3(-12f, 5.13f, 4.16f);
        Vector3 p4 = new Vector3(-13.11f, 5.13f, 4.16f);

        List<Vector3> positions = new List<Vector3>(); // Create a new List
        positions.Add(p1); // Add the Vector3 objects to the List
        positions.Add(p2);
        positions.Add(p3);
        positions.Add(p4);
        if (!isInstantiated)
        {
            foreach (CodeSO c in Codes)
            {

                GameObject cd = Instantiate(c.codePrefab);

                cd.transform.position = positions[counter];
                cd.name = c.codeName;
                counter++;
            }
            isInstantiated = true;
        }
        else
        {
            Debug.Log("codes already in the scene");
        }
    }

}