using UnityEngine;
using UnityEngine.UI;

public class PanelSwitcher : MonoBehaviour
{
    public GameObject firstPanel;
    public GameObject secondPanel;

    private void Start()
    {
        // Ensure the initial state is set correctly
        firstPanel.SetActive(true);
        secondPanel.SetActive(false);
    }

    public void SwitchToFirstPanel()
    {
        firstPanel.SetActive(true);
        secondPanel.SetActive(false);
    }

    public void SwitchToSecondPanel()
    {
        firstPanel.SetActive(false);
        secondPanel.SetActive(true);
    }
}
