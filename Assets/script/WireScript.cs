using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

public class CableController : MonoBehaviour
{
    
    public WireTypeSO WireType;
    public int length;
    private GameObject anchor;
    private List<GameObject> envelopingCylinder = new List<GameObject>();
    private  List<GameObject> sphereChain = new List<GameObject>();

    public void SetWireType(WireTypeSO wireType)
    {
        this.WireType = wireType;
    }
    public WireTypeSO SetWireType()
    {
        return this.WireType;
    }
    void OnEnable()
    {   
        sphereChain = InstantiateJoints(WireType.firstSocketPrefab, WireType.secondSocketPrefab, WireType.spherePrefab ,length);
        //attach joint to each others
        
        for(int i=0; i<length-1; i++){
            ConfigureJoint(sphereChain[i].GetComponent<ConfigurableJoint>(), sphereChain[i+1].GetComponent<Rigidbody>());
        }

        //sphereChain[length-1].GetComponent<Rigidbody>().useGravity = false;
        sphereChain[length-1].GetComponent<Rigidbody>().isKinematic = true;
        for(int i=0; i<length-1; i++){
            CreateEnvelopingCylinder(sphereChain[i],sphereChain[i+1],WireType.cylinderPrefab);
        }
    }
    void Update()
    {
        for(int i=0; i<length-1; i++){
            UpdateEnvelopingCylinder(sphereChain[i],sphereChain[i+1], envelopingCylinder[i]);
        }
    }


    public List<GameObject> InstantiateJoints(GameObject firstSocketPrefab, GameObject secondSocketPrefab, GameObject jointPrefab ,int length)
    {
        List<GameObject> sphereChain = new List<GameObject>();
        float inc = 0.0f;
        for(int i=0; i<length; i++){
            Vector3 incrementPos = new Vector3(inc,0f,0f);
            if(i==0){
                GameObject sp = Instantiate(firstSocketPrefab, transform.position + incrementPos,transform.rotation);
                sp.transform.parent = gameObject.transform;
                sphereChain.Add(sp);
                inc += 0.2f;
            }
            else if(i== (length - 1)){
                GameObject sp = Instantiate(secondSocketPrefab, transform.position + incrementPos,transform.rotation);
                sp.transform.parent = gameObject.transform;
                sphereChain.Add(sp);
                inc += 0.2f;
            }
            else{
                GameObject sp = Instantiate(jointPrefab, transform.position + incrementPos,transform.rotation);
                sp.transform.parent = gameObject.transform;
                sphereChain.Add(sp);
                inc += 0.2f;
            }
        }
        return sphereChain;
    }
    public void ConfigureJoint(ConfigurableJoint j, Rigidbody c)
    {
        j.xMotion = ConfigurableJointMotion.Locked;
        j.yMotion = ConfigurableJointMotion.Locked;
        j.zMotion = ConfigurableJointMotion.Locked;
        j.angularZMotion = ConfigurableJointMotion.Limited;
        j.angularYMotion = ConfigurableJointMotion.Limited;
        j.angularXMotion = ConfigurableJointMotion.Limited;
        j.projectionMode = JointProjectionMode.PositionAndRotation;
        j.enablePreprocessing = false;
        j.connectedBody = c;
    }

    void CreateEnvelopingCylinder(GameObject firstSphere ,GameObject secondSphere, GameObject cylinderPrefab)
    {

            GameObject cy = Instantiate(cylinderPrefab, Vector3.zero, Quaternion.identity);
            //UpdateEnvelopingCylinder(sphereChain[i], sphereChain[i+1],cy);
            cy.transform.parent = gameObject.transform;
            envelopingCylinder.Add(cy);        
    }

    void UpdateEnvelopingCylinder(GameObject firstSphere ,GameObject secondSphere, GameObject cylinder)
    {
            // Calculate the center and height of the cylinder
            Vector3 cylinderCenter = (firstSphere.transform.position + secondSphere.transform.position) / 2f;
            float cylinderHeight = Vector3.Distance(firstSphere.transform.position, secondSphere.transform.position);

            // Set the position and scale of the cylinder
            cylinder.transform.position = cylinderCenter;
            cylinder.transform.up = secondSphere.transform.position - firstSphere.transform.position;
            cylinder.transform.localScale = new Vector3(0.03f, cylinderHeight / 2f,0.03f);
    }
}


