using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HandAnimation : MonoBehaviour
{
    [SerializeField] private InputActionProperty triggerAction;
    [SerializeField] private InputActionProperty gripAction;
    private Animator animator;
    private int triggerHash;
    private int gripHash;

    private void Start()
    {
        animator = GetComponent<Animator>();
        triggerHash = Animator.StringToHash("Trigger");
        gripHash = Animator.StringToHash("Grip");
    }

    private void Update()
    {
        float triggerValue = triggerAction.action.ReadValue<float>();
        float gripValue = gripAction.action.ReadValue<float>();
        animator.SetFloat(triggerHash, triggerValue);
        animator.SetFloat(gripHash, gripValue);
    }
}