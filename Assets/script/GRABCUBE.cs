using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class GrabbedObjectNotifier : MonoBehaviour
{

    ///public Canvas myCanvas;

    private XRGrabInteractable grabInteractable;

    private void Start()
    {

        //myCanvas.enabled = false;
        // Get the XRGrabInteractable component on this object
        grabInteractable = GetComponent<XRGrabInteractable>();

        // Subscribe to the onSelectEntered event
        grabInteractable.onSelectEntered.AddListener(OnGrabbed);

        //myCanvas = GameObject.Find("HandMenuCanvas").GetComponent<Canvas>();
    }

    private void OnGrabbed(XRBaseInteractor interactor)
    {
        //myCanvas.enabled = true;
        // This method will be called when the object is grabbed
        Debug.Log("Object is being grabbed: " + gameObject.name);

        // You can add any other logic or messages you want when the object is grabbed
    }

    private void OnDestroy()
    {
        // Unsubscribe from the event to prevent memory leaks
        grabInteractable.onSelectEntered.RemoveListener(OnGrabbed);
    }
}