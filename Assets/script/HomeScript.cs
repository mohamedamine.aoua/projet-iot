using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public string targetSceneName;

    public void ChangeSceneOnClick()
    {
        Debug.Log("clickeddd");
        SceneManager.LoadScene(targetSceneName);
    }
}