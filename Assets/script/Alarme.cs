using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Alarme : MonoBehaviour
{
   
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        // Assign the AudioClip to the AudioSource
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.name.Equals("zonealarme"))
        {
            audioSource.Play();
        }
    }
  
}
